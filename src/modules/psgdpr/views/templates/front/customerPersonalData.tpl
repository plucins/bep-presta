{**
 * 2007-2020 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

{block name='page_title'}
    {l s='RODO - Dane osobowe' mod='psgdpr'}
{/block}

{block name='page_content'}
<div class="container">
    <section class="page_content">
        <div class="col-xs-12 psgdprinfo17">
            <h2>{l s='Dostęp do moich danych' mod='psgdpr'}</h2>
            <p>{l s='W każdej chwili masz prawo do odzyskania danych, które podałeś na naszej stronie. Kliknij „Pobierz moje dane”, aby automatycznie pobrać kopię swoich danych osobowych w pliku pdf lub csv.' mod='psgdpr'}</p>
            <a id="exportDataToCsv" class="btn btn-primary psgdprgetdatabtn17" target="_blank" href="{$psgdpr_csv_controller|escape:'htmlall':'UTF-8'}">{l s='POBIERZ MOJE DANE JAKO CSV' mod='psgdpr'}</a>
            <a id="exportDataToPdf" class="btn btn-primary psgdprgetdatabtn17" target="_blank" href="{$psgdpr_pdf_controller|escape:'htmlall':'UTF-8'}">{l s='POBIERZ MOJE DANE JAKO PDF' mod='psgdpr'}</a>
        </div>
        <div class="col-xs-12 psgdprinfo17">
            <h2>{l s='Prośby o sprostowanie i usunięcie' mod='psgdpr'}</h2>
            <p>{l s='Masz prawo do zmiany wszystkich danych osobowych znajdujących się na stronie "Moje konto". W przypadku jakichkolwiek innych żądań dotyczących sprostowania i/lub usunięcia danych osobowych, prosimy o kontakt za pośrednictwem naszej' mod='psgdpr'} <a href="{$psgdpr_contactUrl|escape:'htmlall':'UTF-8'}">{l s='strony kontaktowej' mod='psgdpr'}</a>. {l s='Rozpatrzymy Twoją prośbę i odpowiemy tak szybko, jak to możliwe.' mod='psgdpr'}</p>
        </div>
    </section>
</div>
{literal}
<script type="text/javascript">
    var psgdpr_front_controller = "{/literal}{$psgdpr_front_controller|escape:'htmlall':'UTF-8'}{literal}";
    var psgdpr_id_customer = "{/literal}{$psgdpr_front_controller|escape:'htmlall':'UTF-8'}{literal}";
    var psgdpr_ps_version = "{/literal}{$psgdpr_ps_version|escape:'htmlall':'UTF-8'}{literal}";
</script>
{/literal}
{/block}
