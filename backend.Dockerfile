FROM php:7.2-fpm as bep_backend

RUN : \
    && apt-get update -y \
    && apt-get -y install \
        gcc make autoconf libc-dev pkg-config libzip-dev zip wget nano \
        libfreetype6-dev libjpeg62-turbo-dev libpng-dev libicu-dev \
    && docker-php-ext-configure gd \
        --with-freetype-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install zip pdo pdo_mysql intl \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && :

COPY config/php.ini /usr/local/etc/php/php.ini

COPY --chown=www-data:www-data src /var/www/shop

RUN mkdir -p /var/www/shop/var/cache && mkdir -p /var/www/shop/var/logs
RUN chown -R www-data:www-data /var/www/shop/var/

WORKDIR /var/www/shop

RUN chmod 777 /var/www/shop/var/cache
