# BEP-presta

Shop items where scraped by <https://gitlab.com/plucins/lamp-scraper>
app.

## Deployment instruction

**Connect to VPN before deployment!**

    ssh rsww@172.20.83.101
    ssh hdoop@actina15.maas
    cd /mnt/block-storage/students/projects/students-swarm-services/BE_131514/src/bep-presta
    git pull
    ./deploy.sh

## How to run?

Build Docker images:

    docker compose build

Start containers:

    docker compose up

Connect to SSH and forward port 443 (on Unix root privileges are
required to forward onto port 443):

    ssh -L 443:actina15.maas:13151 rsww@172.20.83.101

Open <https://localhost/> in your browser address bar. It should
redirect you to the shop webpage (you will see a warning because of
self-signed certificate: `NET::ERR_CERT_AUTHORITY_INVALID`):

![shop webpage](images/shop-webpage.png)

## Access to Adminer

Adminer allows to view and edit database via web browser. You need to
set up a SSH tunnel for port 9099:

    ssh -L 9099:actina15.maas:9099 rsww@172.20.83.101

And open <http://localhost:9099/>. Login details as follows:

* System: `MySQL`
* Server: `db` (should be entered by default)
* Username: `root`
* Password: on page 14 of [this document][1]
* Database: `BE_131514`

## SSH password

SSH passwords for `rsww@172.20.83.101` and `hdoop@actina15.maas` are on
page 7 of [this document][1].

[1]: https://enauczanie.pg.edu.pl/moodle/pluginfile.php/1473279/mod_resource/content/1/instrukcja%20klaster%20studenci.pdf
