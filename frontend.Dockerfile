FROM nginx:latest as bep_webserver

COPY config/nginx.conf /etc/nginx/conf.d/default.conf

COPY --chown=nginx:nginx config/ssl.crt /etc/ssl/fullchain.pem
COPY --chown=nginx:nginx config/ssl.key /etc/ssl/privkey.pem
RUN chmod 600 /etc/ssl/*.pem

COPY src /var/www/shop
